import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Posts } from './interface/posts';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  apiurl = "https://jsonplaceholder.typicode.com/posts"

  constructor(private _http: HttpClient) { }

  getPost(){
    return this._http.get<Posts[]>(this.apiurl);
  }
}
